package server;

import io.javalin.Javalin;
import io.javalin.http.staticfiles.Location;

public class AirlineServer {
    private static final String PAGES_DIR = "/public";

    private final Javalin appServer;

    public AirlineServer() {
        appServer = Javalin.create(config -> config.addStaticFiles(PAGES_DIR, Location.CLASSPATH));
    }

    public static void main(String[] args) {
        AirlineServer server = new AirlineServer();
        server.start(5050);
    }

    public void start(int port) {
        this.appServer.start(port);
    }

    public void stop() {
        this.appServer.stop();
    }

    public int port() {
        return appServer.port();
    }
}