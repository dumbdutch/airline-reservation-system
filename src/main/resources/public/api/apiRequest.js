function connectApi() {
    const options = {
        method: 'GET',
        headers: {
            'X-RapidAPI-Key': '9cc77552a0mshe47bf7f3d05eacdp1c8137jsn9c518133c320',
            'X-RapidAPI-Host': 'aerodatabox.p.rapidapi.com'
        }
    };
    
    fetch('https://aerodatabox.p.rapidapi.com/flights/%7BsearchBy%7D/DL47?withAircraftImage=false&withLocation=false', options)
        .then(response => response.json())
        .then(response => console.log(response))
        .catch(err => console.error(err));
}