import errorTemplate from "../views/error.js"
import { searchHandler } from "../model/search.js"
import { searchTemplate } from "../views/search.js"

const app = document.getElementById('app');
const main = $('#app');


let  router = new Router({
    mode : 'hash' ,
    root: 'index.html' ,
    page404: () => {
        app.innerHTML = errorTemplate.call()
        console.log( 'Error 404 ')
        }
    })

router.add('/', searchHandler.bind( { router : router , data : searchTemplate.call() } ) )

export default router